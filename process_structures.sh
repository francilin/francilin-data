#!/bin/bash
#
# Process structures CSV file into GeoJSON file
#
# Pierre Dittgen, Jailbreak
#


TMP_DIR=$(mktemp --directory --suffix=.francilin)
CURRENT_PATH=$(dirname $PWD/$0)

norm_csv() {
    source_csv=$1
    target_csv=$2

    python3 $CURRENT_PATH/scripts/norm_csv.py $source_csv $target_csv
}


add_geo_coords() {
    source_csv=$1
    target_csv=$2

    geo_2=$TMP_DIR/geo_2.csv
    geo_2_req=$TMP_DIR/geo_2_req
    geo_2_res=$TMP_DIR/geo_2_res
    geo_3=$TMP_DIR/geo_3.csv
    geo_4=$TMP_DIR/geo_4.csv

    if [ ! -e $geo_2_res ]
    then
      mkdir $geo_2_res
    fi

    FRANCILIN_ID_COL="francilin_id"

    # Then extract wanted columns
    xsv select "\"$FRANCILIN_ID_COL\",\"adresse\",\"commune\",\"code_postal\"" $source_csv > $geo_2

    # Then call geo api
    xsv split -s 300 $geo_2_req $geo_2
    for geo_2_splitted in $geo_2_req/*.csv
    do
      echo $geo_2_splitted
      curl -X POST -F data="@$geo_2_splitted" -F columns=adresse -F columns=commune -F postcode=code_postal https://api-adresse.data.gouv.fr/search/csv/ > $geo_2_res/$(basename $geo_2_splitted)
      sleep 1
    done

    xsv cat rows $geo_2_res/*.csv > $geo_3

    # Only keep $FRANCILIN_ID_COL and latitude, longitude
    xsv select "\"$FRANCILIN_ID_COL\",\"latitude\",\"longitude\"" $geo_3 > $geo_4

    # Join files
    xsv join $FRANCILIN_ID_COL $source_csv $FRANCILIN_ID_COL $geo_4 > $target_csv
}

convert_to_geojson() {
    source_csv=$1
    target_geojson=$2

    python3 $CURRENT_PATH/scripts/generate_jsons.py $source_csv $target_geojson
}

#### main ####

if [ $# -eq 0 ]; then
    # get the number of source files.
    NB_SOURCE_FILES=$(ls src/source_fichier_normalise*.csv | wc -l)

    if [ $NB_SOURCE_FILES -eq 0 ]; then
        echo "you must have at least one src/source_fichier_normalise*.csv item"
        exit 1
    elif [ $NB_SOURCE_FILES -eq 1 ]; then
        SOURCE_CSV=$(ls src/source_fichier_normalise*.csv)
    else
        # src/source_fichier_normalise-2021-05-06.csv
        # src/source_fichier_normalise-2021-07-06.csv
        # src/source_fichier_normalise-2021-07-07.csv <-- take the penultimate element
        # src/source_fichier_normalise.csv
        SOURCE_CSV=$(ls src/source_fichier_normalise*.csv | sort | tail -2 | head -1)
    fi

    SOURCE_FILENAME=$(basename $SOURCE_CSV .csv)
    TARGET_GEOJSON="dist/structures${SOURCE_FILENAME:24}.geojson"

    echo "source: $SOURCE_CSV"
    echo "dest:   $TARGET_GEOJSON"
elif [ $# -ne 2 ]; then
    echo "usage: $0 <source_fichier_normalise.csv> <structures.geojson>"
    exit 1
else
    SOURCE_CSV="$1"
    TARGET_GEOJSON="$2"
fi

if [ "$SOURCE_CSV" == "" -o ! -e "$SOURCE_CSV" ]; then
    echo "Source CSV file not found: $SOURCE_CSV"
    exit 1
fi

if [ "$TARGET_GEOJSON" == "" ]; then
    echo "Undefined target GeoJSON file"
    exit 1
fi

mkdir -p $TMP_DIR
norm_csv $SOURCE_CSV $TMP_DIR/temp_1.csv
add_geo_coords $TMP_DIR/temp_1.csv $TMP_DIR/temp_2.csv
convert_to_geojson $TMP_DIR/temp_2.csv $TARGET_GEOJSON
rm -fR $TMP_DIR