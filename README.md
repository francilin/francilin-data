# Données Francil'IN

[Francil'IN](www.francilin.fr) recense les lieux d'inclusion numérique en Ile-de-France. Ce dépôt a vocation à héberger les données propres à ce recensement.

La liste des lieux d'inclusion numérique d'Île de France est disponible dans un tableur nommé `Fichiers Draft Carto IN IdF` au sein d'un synology avec accès authentifié.  
Ce tableur est maintenu par l'équipe Francil'IN, il comprend un onglet
`SOURCE - Fichier normalisé` contenant 1 lieu par ligne.

Pour les besoins du projet, le fichier des lieux d'inclusion est copié à intervalle régulier dans le dossier `src` (sous le nom `source_fichier_normalise.csv`). Le dossier `dist` de ce même projet contient le fichier `structures.geojson` utilisé par le projet [francilin-carto](https://gitlab.com/francilin/francilin-carto)

## Préparation des données

- se connecter sur l'interface synology
- ouvrir le tableur
- exporter le tableur en ligne (Synology, URL privée) "Fichiers Draft Carto IN Idf" au format Excel (.xlsx)
- importer le fichier dans LibreOffice Calc :
  - exporter en CSV l'onglet `SOURCE - fichier normalisé` dans `src/source_fichier_normalise-YYYY-MM-DD.csv` (avec la date du jour), en prenant soin de ne pas exporter les colonnes tout à droite qui sont une aide destinée aux humains

Remarque : le champ description peut contenir des retours à la ligne qui peuvent perturbées la mise en forme du csv et son interprétation.
Il peut donc être nécessaire de remplacer les retours à la ligne par des `\n` explicites. (Rechercher et remplacer : `([^,])\n` par `$1\\n`) sauf pour la colonne U.

## Traitement de données Francil'IN

La transformation des données concernant les lieux d'inclusion numérique d'Île de France permet :

- la géolocalisation automatique
- la transformation de la source CSV en JSON

### Dépendances techniques

- bash
- python3 (virtual env)
- xsv
- curl

### Génération du fichier GeoJSON

La génération est automatisée avec le script `process_structures.sh`. Appelé sans arguments il va chercher le fichier `src/source_fichier_normalise-YYYY-MM-DD.csv` le plus récent et générer le fichier GeoJSON dans `dist`.

```bash
./process_structures.sh

# vous pouvez également préciser le fichier source et destination
./process_structures.sh <source.csv> <destination.geojson>
```

Cela va créer un fichier `structures-YYYY-MM-DD.geojson` dans le dossier `dist`.

### Vérification des données

Vous pouvez lancer un serveur sur ce même dossier afin de tester localement ces nouvelles données dans l'application cartographique.

```console
npm i
npm run serve
```

lancera un serveur local sur le dossier `dist` avec les en-têtes CORS requises.

Il suffit alors de renseigner l'url des données dans la variable d'environnement `STRUCTURES_URL` du fichier `.env` de [Francil'IN Carto](https://gitlab.com/francilin/francilin-carto).

```bash
STRUCTURES_URL=http://127.0.0.1:8080/structures-YYYY-MM-DD.geojson
```

## Envoi des données

Si les données sont correctes vous pouvez copier le fichier à jour `structures-YYYY-MM-DD.geojson` dans `structures.geojson`.

```console
./replace_principal_files.sh
```

et faire un commit.

lorsque la migration du site sur le netlify de Francil'IN sera effectuée,
tout envoi de données sur la branche master déclenche :

- un déploiement de ces nouvelles données sur : https://data.francilin.fr
- un redéploiement du site : https://carto.francilin.fr Ceci afin de regénérer toutes les pages statiques avec le contenu mis à jour.

## Développement

Alors que la branche principale ne contient que la dernière version des données. La branche dev contient l'historique des différentes versions et peut contenir également des fichiers pour utiles pour le débogage (pour tester les filtres notamment).

La branche de dev est également déployée sur Netlify à cette adresse : https://dev--data-francilin.netlify.app