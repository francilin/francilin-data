#!/bin/bash

SRC_LIST=$(ls src/source_fichier_normalise-*.csv | sort -r)
DIST_LIST=$(ls dist/structures-*.geojson | sort -r)

PS3='Fichier à copier dans src/source_fichier_normalise.csv ? '
select SRC_FILE in $SRC_LIST; do
  if [ "$SRC_FILE" ] && [ -f "$SRC_FILE" ]; then
    printf "cp %s src/source_fichier_normalise.csv\n\n" "$SRC_FILE"
    cp "$SRC_FILE" src/source_fichier_normalise.csv
  fi
  break
done

PS3='Fichier à copier dans dist/structures.geojson ? '
select DIST_FILE in $DIST_LIST; do
  if [ "$DIST_FILE" ] && [ -f "$DIST_FILE" ]; then
    printf "cp %s dist/structures.geojson\n" "$DIST_FILE"
    cp "$DIST_FILE" dist/structures.geojson
  fi
  break
done
